package apiserver

import (
	"github.com/gin-gonic/gin"
	"github.com/mediasoft-upload-images/internal/app"
	"github.com/mediasoft-upload-images/internal/app/model"
	"github.com/mediasoft-upload-images/store"
)

// APIServer ...
type APIServer struct {
	Config  *app.Config
	store   *store.Store
	CloseDb *store.Store
}

// New ...
func New(config *app.Config) *APIServer {
	return &APIServer{
		Config: config,
	}
}

// Start ...
func (s *APIServer) Start() error {

	router := gin.Default()

	if err := s.configureStore(); err != nil {
		panic(err)
	}
	s.configureRouters(router)
	return router.Run(s.Config.Port)
}

func (s *APIServer) configureStore() error {
	st := store.New(s.Config)
	if err := st.Open(); err != nil {
		return err
	}

	s.store = st
	return nil
}

func (s *APIServer) configureRouters(routes *gin.Engine) {

	v1 := routes.Group("v1")
	{
		v1.POST("/register", s.Register)
		v1.POST("/login", s.Login)
		v1.GET("/users", s.AuthMiddleware, s.Index)
	}

}

// AuthMiddleware ...
func (s *APIServer) AuthMiddleware(c *gin.Context) {
	_, err := c.Cookie("auth_key")

	if err != nil {
		c.JSON(401, gin.H{
			"message": "Вы не авторизован",
		})
	} else {
		c.Next()
	}
}

// Index ...
func (s *APIServer) Index(c *gin.Context) {
	users, err := s.store.User().GetAll()
	if err != nil {
		panic(err)
	}
	c.JSON(200, gin.H{
		"message": "пользователи",
		"users":   users,
	})
}

// Register ..
func (s *APIServer) Register(c *gin.Context) {
	email := c.PostForm("email")
	password := c.PostForm("password")
	count, err := s.store.User().FindByEmail(email)
	if err != nil {
		panic(err)
	}

	if count > 0 {
		c.JSON(401, gin.H{
			"message": "Пользователь с таким мылом уже существует",
		})
	} else {
		_, err := s.store.User().Create(&model.User{Email: email, Password: password})
		if err != nil {
			panic(err)
		}
		var generetedToken string = GenerateToken()
		//  set user token
		c.SetCookie(
			"auth_key",
			generetedToken,
			3600,
			"/",
			"127.0.0.1",
			false,
			false,
		)

		c.JSON(200, gin.H{
			"auth_key": generetedToken,
		})
	}
}

// Login ...
func (s *APIServer) Login(c *gin.Context) {

	email := c.PostForm("email")
	password := c.PostForm("password")

	count, err := s.store.User().FindUser(email, password)
	if err != nil {
		panic(err)
	}

	if count == 0 {
		c.JSON(401, gin.H{
			"message": "Такого юсера и в помине нет.",
		})
	} else {
		var generetedToken string = GenerateToken()
		//  set user token
		c.SetCookie(
			"auth_key",
			generetedToken,
			3600,
			"/",
			"127.0.0.1",
			false,
			false,
		)

		c.JSON(200, gin.H{
			"auth_key": generetedToken,
		})
	}
}
