module github.com/mediasoft-upload-images

go 1.15

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/gin-contrib/sessions v0.0.3
	github.com/gin-gonic/gin v1.6.3
	github.com/go-playground/validator/v10 v10.2.0
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/lib/pq v1.8.0 // indirect
)
