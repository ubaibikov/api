package store

import (
	"database/sql"
	"fmt"

	"github.com/mediasoft-upload-images/internal/app"
)

// Store db
type Store struct {
	db             *sql.DB
	userRepository *UserRepository
	Config         *app.Config
}

// New ...
func New(config *app.Config) *Store {
	return &Store{
		Config: config,
	}
}

// Open ...
func (s *Store) Open() error {
	pqConfig := dbConfig(s)
	db, err := sql.Open("postgres", pqConfig)
	if err != nil {
		return err
	}

	if err := db.Ping(); err != nil {
		return err
	}

	s.db = db
	return nil
}
func dbConfig(s *Store) string {
	return fmt.Sprintf(`
    user=%s 
    port=%s 
    host=%s 
    password=%s 
    dbname=%s 
    sslmode=%s`,
		s.Config.DB_User,
		s.Config.DB_PORT,
		s.Config.DB_Host,
		s.Config.DB_Password,
		s.Config.DB_Name,
		s.Config.DB_SSlmode)
}

// Close ...
func (s *Store) Close() {
	s.db.Close()
}

// User ...
func (s *Store) User() *UserRepository {
	if s.userRepository != nil {
		return s.userRepository
	}

	s.userRepository = &UserRepository{
		store: s,
	}
	return s.userRepository
}
