package main

import (
	"flag"
	"log"

	"github.com/BurntSushi/toml"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"github.com/mediasoft-upload-images/internal/app"
	"github.com/mediasoft-upload-images/internal/app/apiserver"
)

var (
	configPath string
)

func init() {
	err := godotenv.Load()
	if err != nil {
		panic(err)
	}

	flag.StringVar(&configPath, "config-path", "configs/apiserver.toml", "path to config file")
}

func main() {

	flag.Parse()

	config := app.NewConfig()

	_, err := toml.DecodeFile(configPath, config)
	if err != nil {
		log.Fatal(err.Error())
	}

	s := apiserver.New(config)

	if err := s.Start(); err != nil {
		log.Fatal(err)
	}
}
